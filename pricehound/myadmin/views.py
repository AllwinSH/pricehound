# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from .models import ProductModel
# from django.conf import settings
# from django.core.mail import send_mail

# send_mail(
#     'Test Mail from Django',
#     'Hi There, I am your Pricehound, How can i help you?',
#     settings.EMAIL_HOST_USER,
#     settings.ADMIN,
#     fail_silently=False
# )
from django.shortcuts import get_list_or_404, get_object_or_404

def update_price(request):

    products = ProductModel.objects.all()
    return render(request, 'pricehound_admin/product_list.html', {'products':products})

from myadmin.models import ProductModel,ProductVariant,ProductListing,Brand,Category,Store
from django.core.management.base import BaseCommand
import csv


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        # Since the CSV headers match the model fields,
        # you only need to provide the file's path
        # ProductModel.objects.from_csv('/home/unknown/UnoTech/pricehound/modelsToUpload_Apple.csv',
		# 			dict(brand='Brand', name='Model',category='Category',description='Descritpion',specifications='Specification'))

        # ProductVariant.objects.from_csv('/home/unknown/UnoTech/pricehound/variantsToUpload_Apple.csv',
        # 			dict(product_model='Model', color='Color',capacity='Capacity',images='images'))

        # ProductListing.objects.from_csv('/home/unknown/UnoTech/pricehound/variantsToUpload_Apple.csv',
        # 			dict(product_variant='Variant', store='Store',product_id='Capacity'))

        # For Importing ProductModel into database
        first_frame=1
        with open('/home/unknown/UnoTech/pricehound/modelsToUpload_Apple.csv','rU') as f:
            data = csv.reader(f)
            for row in data:

                if(first_frame==1):
                    first_frame=0
                    continue

                Primary_Key_Brand= Brand.objects.get(name=row[0])
                Primary_Key_Category=Category.objects.get(name=row[2])
                product= ProductModel(brand=Primary_Key_Brand,name=row[1],category=Primary_Key_Category)
                product.save()
                print product

        # For Importing ProductVariants into database
        first_frame=1
        with open('/home/unknown/UnoTech/pricehound/variantsToUpload_Apple.csv','rU') as f:
            data = csv.reader(f)
            for row in data:

                if(first_frame==1):
                    first_frame=0
                    continue

                Primary_Key_Model= ProductModel.objects.all()

                for products in Primary_Key_Model:
                    if(str(products)==row[0]):
                        product= ProductVariant(product_model=products,color=row[1],capacity=row[2])
                        product.save()
                        print product

        # For Importing ProductListing into database
        first_frame=1
        with open('/home/unknown/UnoTech/pricehound/listingsToUpload_Apple.csv','rU') as f:
            data = csv.reader(f)
            for row in data:

                if(first_frame==1):
                    first_frame=0
                    continue

                Primary_Key_Variant= ProductVariant.objects.all()

                for products in Primary_Key_Variant:
                    if(str(products)==row[0]):
                        Primary_Key_Store=Store.objects.get(name=row[1])
                        product= ProductListing(product_variant=products,store=Primary_Key_Store,product_id=row[2])
                        product.save()
                        print product
